﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Spotify;
using Spotify.Interfaces;

namespace HobbySite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpotifyController : Controller
    {
        private readonly ISpotifyService _spotifyService;
        public SpotifyController(ISpotifyService spotifyService)
        {
            _spotifyService = spotifyService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [HttpGet("login")]
        public IActionResult Login()
        {

            string redirecturl = _spotifyService.Login();
            return Redirect(redirecturl);
        }

        [HttpPost("callback")]
        [HttpGet("callback")]
        public IActionResult Callback()
        {
            var queryDictionary = System.Web.HttpUtility.ParseQueryString(Request.Path);
            return Json(new { Access_Token = queryDictionary["access_token"] });
        }

        [HttpPost]
        [HttpGet("artist")]
        public IActionResult Artist(string id)
        {
            return null;
        }
    }
}
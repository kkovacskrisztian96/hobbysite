﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Spotify
{
    public class Core
    {
        IConfiguration _config;
        public Core(IConfiguration config)
        {
            _config = config;
        }

        public HttpRequestMessage GetLoginRequest()
        {
            HttpRequestMessage request = new HttpRequestMessage();

            string loginRequestUrl = _config.GetSection("Spotify").GetSection("Authorization").GetSection("Url").Value;
            string endpoint = _config.GetSection("Spotify").GetSection("Authorization").GetSection("Endpoints").GetSection("Authorize").Value;

            string url = loginRequestUrl + endpoint + "?";
            Dictionary<string, string> headers = GetAuthorizationRequestHeaders();
            foreach (var item in headers)
            {
                url += item.Key + "=" + item.Value + "&";
                //request.Headers.Add(item.Key, item.Value);
            }
            url = url.TrimEnd('&');
            Uri requestUri = new Uri(url);
            request.RequestUri = requestUri;

            return request;
        }

        public HttpRequestMessage GetArtistRequest()
        {
            HttpRequestMessage request = new HttpRequestMessage();

            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Content-Type", "application/json");

            return request;
        }

        public Dictionary<string, string> GetAuthorizationRequestHeaders()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            var headerSection = _config.GetSection("Spotify").GetSection("Authorization").GetSection("Headers");

            foreach (var header in headerSection.GetChildren())
            {
                headers.Add(header.Key, header.Value);
            }

            return headers;
        }
    }
}

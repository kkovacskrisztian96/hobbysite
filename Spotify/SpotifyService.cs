﻿using Microsoft.Extensions.Configuration;
using Spotify.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Spotify
{
    public class SpotifyService : ISpotifyService
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IConfiguration _config;
        private Core core;

        public SpotifyService(IHttpClientFactory clientFactory, IConfiguration config)
        {
            _clientFactory = clientFactory;
            _config = config;
            core = new Core(_config);
        }

        /// <summary>
        /// Returns the 
        /// </summary>
        /// <returns></returns>
        public string Login()
        {
            HttpRequestMessage request = core.GetLoginRequest();

            using (var client = _clientFactory.CreateClient())
            {
                var response = client.SendAsync(request).Result;
                string redirectUrl = Uri.UnescapeDataString(response.RequestMessage.RequestUri.Query.Replace("?continue=",""));

                return redirectUrl;
            }
        }

        public string GetArtist(string id)
        {
            return "";
        }
    }
}

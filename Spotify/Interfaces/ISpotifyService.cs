﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Spotify.Interfaces
{
    public interface ISpotifyService
    {
        /// <summary>
        /// Returns the URL for the user to log into Spotify.
        /// </summary>
        /// <returns></returns>
        string Login();
    }
}
